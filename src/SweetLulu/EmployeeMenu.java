/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SweetLulu;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Charlie
 */
public class EmployeeMenu extends javax.swing.JFrame {
    DefaultTableModel model = new DefaultTableModel();
    getSum sum=new getSum();
    table2 JT = new table2();    
    PreparedStatement ps;
    SearchTable jTable = new SearchTable();
    String expenses = "SELECT `Date`, `Item`, `Cost` FROM `expenses` WHERE `Item` LIKE ?";
    String te="SELECT `Date` ,`Item`, `Cost` FROM `expenses` WHERE STR_TO_DATE(Date,'%Y-%m-%d') BETWEEN STR_TO_DATE(?,'%Y-%m-%d') AND STR_TO_DATE(?,'%Y-%m-%d')";
    
    public EmployeeMenu() {
       
        initComponents();
    }
    
    public EmployeeMenu(String AccessType) {
        this.setExtendedState(this.getExtendedState() | EmployeeMenu.MAXIMIZED_BOTH);
        initComponents();
        jPanel2.setVisible(false);
        Date date = new Date();
        jDateChooser1.setDate(date);
        jDateChooser5.setDate(date);
         jDateChooser5.setVisible(false);
        FilterTable(jTable3,te,jDateChooser5.getDate(),jDateChooser5.getDate());
        
        jLabel33.setText(AccessType);
        if (AccessType.equals("Employee")) {
        jDateChooser1.setEnabled(false);
        jDateChooser2.setEnabled(false);
        }
         jLabel33.setVisible(false);
         Date();
        showTime();
        showDate();
        cbo();
        cbo2();
        nullItem_price();
    }
    
    public void Date() {
Date date = new Date();
jDateChooser2.setDate(date);
}
    
   public void nullItem_price(){
       // Food null
    if(cbofood.getSelectedItem()=="-select food-" && jComboBox2.getSelectedItem()!="-select drink-"){
    txtPrice.setText("0.00");
}
    // Food and drink null
    else if(jComboBox2.getSelectedItem()=="-select drink-" && cbofood.getSelectedItem()=="-select food-" ){
    txtPrice.setText("0.00");
    txtPrice2.setText("0.00");
}
    // Drink null
    else if(jComboBox2.getSelectedItem()=="-select drink-" && cbofood.getSelectedItem()!="-select food-" ){
    txtPrice2.setText("0.00");
}        
   }
    public void price(){
        Connection con = getConnection();
        try{
            PreparedStatement ptst= con.prepareStatement("SELECT `ID`, `Name`, `Price` FROM `menu` WHERE Name='"+cbofood+"'");
            ResultSet res= ptst.executeQuery();
               txtPrice.setText(res.getString("Price"));
        }catch(SQLException e){
            
        }
    }
    
     Connection getConnection(){
        Connection con=null;
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sweetlulu", "root", "");
            return con;
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(this, ex);
            return null;
        }
    
    }
     public void cbo(){
    Connection con= getConnection();
       try{
              ps = con.prepareStatement("SELECT * FROM `menu`"); 
            ResultSet result = ps.executeQuery();
           while(result.next()){
               String hint=result.getString("Name");
               cbofood.addItem(hint);
           }
       }catch(SQLException ex){
           JOptionPane.showMessageDialog(this, ex);
       } catch (Exception e){
           JOptionPane.showMessageDialog(this, e);
       } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
       }
}
      public void cbo2(){
    Connection con= getConnection();
       try{
              ps = con.prepareStatement("SELECT * FROM `drinks`"); 
            ResultSet result = ps.executeQuery();
           while(result.next()){
               String hint=result.getString("Name");
               jComboBox2.addItem(hint);
               
           }
       }catch(SQLException ex){
           JOptionPane.showMessageDialog(this, ex);
       } catch (Exception e){
           JOptionPane.showMessageDialog(this, e);
       } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
       }
}
public void showDate(){
    Date d = new Date();
    SimpleDateFormat s= new SimpleDateFormat("dd-MM-yyyy");
    jLabel1.setText(s.format(d));
}

public void showTime(){
    new Timer(0,new ActionListener(){
        @Override
        public void actionPerformed(ActionEvent ae) {
            Date d =new Date();
            SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
           jLabel2.setText(s.format(d));//To change body of generated methods, choose Tools | Templates.
        }
    }).start();
}

    public void FilterTable(JTable table, String query, Date date01, Date date02) {
    Connection con = getConnection();
    try {
    PreparedStatement ps = con.prepareStatement(query);
    java.sql.Timestamp date1 = new java.sql.Timestamp(date01.getTime());
    java.sql.Timestamp date2 = new java.sql.Timestamp(date02.getTime());
    ps.setTimestamp(1, date1);
    ps.setTimestamp(2, date2);
   ResultSet rs = ps.executeQuery();
     //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        int columns = rs.getMetaData().getColumnCount();
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            ((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
        }
    } catch (SQLException ex) {
    JOptionPane.showMessageDialog(null, ex);
    } finally {
      
    }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SalePayment = new javax.swing.JDialog();
        jPanel20 = new javax.swing.JPanel();
        jLabel53 = new javax.swing.JLabel();
        txtPAmnt = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        txtTendered = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        txtBal = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        btnPrint = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        Custom_Order = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jLabel56 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        Expenses = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel25 = new javax.swing.JLabel();
        totalExpenses = new javax.swing.JTextField();
        txt_search3 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        To = new com.toedter.calendar.JDateChooser();
        jLabel26 = new javax.swing.JLabel();
        From = new com.toedter.calendar.JDateChooser();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        item = new javax.swing.JTextField();
        itemprice = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jButton12 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        cbofood = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnPlace = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        txtPrice2 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        Calculator = new javax.swing.JButton();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        CustomOrder = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        txtPrice1 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtPrice3 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        Quantity2 = new javax.swing.JTextField();
        Quantity1 = new javax.swing.JTextField();
        btnLogout1 = new javax.swing.JButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel33 = new javax.swing.JLabel();
        jDateChooser5 = new com.toedter.calendar.JDateChooser();

        SalePayment.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        SalePayment.setMinimumSize(new java.awt.Dimension(327, 378));
        SalePayment.setModal(true);
        SalePayment.setResizable(false);

        jPanel20.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel20.setLayout(null);

        jLabel53.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel53.setText("Amount Due");
        jPanel20.add(jLabel53);
        jLabel53.setBounds(110, 50, 100, 30);

        txtPAmnt.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtPAmnt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPAmnt.setText("0.0");
        txtPAmnt.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel20.add(txtPAmnt);
        txtPAmnt.setBounds(70, 80, 200, 40);

        jLabel54.setFont(new java.awt.Font("Monotype Corsiva", 0, 34)); // NOI18N
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel54.setText("Sale Payment");
        jLabel54.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel20.add(jLabel54);
        jLabel54.setBounds(0, 0, 330, 40);

        txtTendered.setBackground(new java.awt.Color(46, 66, 99));
        txtTendered.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTendered.setForeground(new java.awt.Color(255, 255, 255));
        txtTendered.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTendered.setText("0.0");
        txtTendered.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtTenderedCaretUpdate(evt);
            }
        });
        jPanel20.add(txtTendered);
        txtTendered.setBounds(130, 220, 180, 30);

        jLabel55.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("Balance");
        jPanel20.add(jLabel55);
        jLabel55.setBounds(110, 130, 100, 30);

        txtBal.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        txtBal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtBal.setText("0.0");
        txtBal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel20.add(txtBal);
        txtBal.setBounds(70, 160, 200, 40);

        jLabel64.setFont(new java.awt.Font("Corbel", 0, 14)); // NOI18N
        jLabel64.setText("Amount Tendered");
        jPanel20.add(jLabel64);
        jLabel64.setBounds(10, 220, 140, 30);

        btnPrint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPrint.setText("Submit");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        jPanel20.add(btnPrint);
        btnPrint.setBounds(90, 280, 130, 50);
        jPanel20.add(jLabel5);
        jLabel5.setBounds(10, 140, 40, 30);

        javax.swing.GroupLayout SalePaymentLayout = new javax.swing.GroupLayout(SalePayment.getContentPane());
        SalePayment.getContentPane().setLayout(SalePaymentLayout);
        SalePaymentLayout.setHorizontalGroup(
            SalePaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        SalePaymentLayout.setVerticalGroup(
            SalePaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SalePaymentLayout.createSequentialGroup()
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        Custom_Order.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Custom_Order.setMinimumSize(new java.awt.Dimension(378, 379));
        Custom_Order.setModal(true);
        Custom_Order.setResizable(false);

        jPanel3.setLayout(null);

        jLabel56.setFont(new java.awt.Font("Monotype Corsiva", 0, 34)); // NOI18N
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("Custom Order");
        jLabel56.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.add(jLabel56);
        jLabel56.setBounds(0, 0, 380, 40);

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Food", "Drink" }));
        jPanel3.add(jComboBox1);
        jComboBox1.setBounds(30, 60, 130, 23);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setText("Item");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(30, 110, 37, 22);

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel22.setText("Quantity");
        jPanel3.add(jLabel22);
        jLabel22.setBounds(30, 150, 67, 22);

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel23.setText("Total");
        jPanel3.add(jLabel23);
        jLabel23.setBounds(30, 240, 45, 22);

        jTextField5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField5.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField5CaretUpdate(evt);
            }
        });
        jPanel3.add(jTextField5);
        jTextField5.setBounds(130, 150, 200, 30);

        jTextField6.setEditable(false);
        jTextField6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField6.setText("0.0");
        jPanel3.add(jTextField6);
        jTextField6.setBounds(130, 240, 200, 30);

        jTextField7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel3.add(jTextField7);
        jTextField7.setBounds(130, 110, 200, 30);

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel24.setText("Price");
        jPanel3.add(jLabel24);
        jLabel24.setBounds(30, 190, 37, 22);

        jTextField8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField8.setText("0.0");
        jTextField8.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField8CaretUpdate(evt);
            }
        });
        jPanel3.add(jTextField8);
        jTextField8.setBounds(130, 190, 200, 30);

        jButton3.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jButton3.setText("Process");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton3);
        jButton3.setBounds(130, 300, 110, 31);

        javax.swing.GroupLayout Custom_OrderLayout = new javax.swing.GroupLayout(Custom_Order.getContentPane());
        Custom_Order.getContentPane().setLayout(Custom_OrderLayout);
        Custom_OrderLayout.setHorizontalGroup(
            Custom_OrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Custom_OrderLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        Custom_OrderLayout.setVerticalGroup(
            Custom_OrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Custom_OrderLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        Expenses.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Expenses.setMinimumSize(new java.awt.Dimension(981, 621));
        Expenses.setModal(true);
        Expenses.setResizable(false);

        jPanel4.setLayout(null);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Expense", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable3);

        jPanel4.add(jScrollPane2);
        jScrollPane2.setBounds(350, 190, 600, 340);

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel25.setText("Total:");
        jPanel4.add(jLabel25);
        jLabel25.setBounds(690, 530, 80, 40);

        totalExpenses.setEditable(false);
        totalExpenses.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        totalExpenses.setText("0.00");
        jPanel4.add(totalExpenses);
        totalExpenses.setBounds(770, 530, 180, 40);

        txt_search3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_search3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_search3CaretUpdate(evt);
            }
        });
        txt_search3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_search3MouseClicked(evt);
            }
        });
        jPanel4.add(txt_search3);
        txt_search3.setBounds(590, 140, 190, 30);

        jLabel32.setForeground(java.awt.Color.blue);
        jLabel32.setText("* Search Item");
        jPanel4.add(jLabel32);
        jLabel32.setBounds(790, 140, 140, 20);

        jButton4.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jButton4.setText("Search");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton4);
        jButton4.setBounds(800, 90, 150, 30);
        jPanel4.add(To);
        To.setBounds(610, 90, 180, 30);

        jLabel26.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel26.setText("To");
        jPanel4.add(jLabel26);
        jLabel26.setBounds(580, 90, 30, 30);
        jPanel4.add(From);
        From.setBounds(400, 90, 170, 30);

        jLabel27.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel27.setText("From");
        jPanel4.add(jLabel27);
        jLabel27.setBounds(360, 90, 40, 30);

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel28.setText("Date");
        jPanel4.add(jLabel28);
        jLabel28.setBounds(40, 190, 70, 30);

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel29.setText("Item");
        jPanel4.add(jLabel29);
        jLabel29.setBounds(40, 240, 70, 30);

        item.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel4.add(item);
        item.setBounds(110, 240, 220, 30);

        itemprice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemprice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itempriceActionPerformed(evt);
            }
        });
        jPanel4.add(itemprice);
        itemprice.setBounds(110, 290, 220, 30);

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel30.setText("Price");
        jPanel4.add(jLabel30);
        jLabel30.setBounds(40, 290, 70, 30);

        jButton12.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton12.setText("Clear Fields");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton12);
        jButton12.setBounds(120, 340, 210, 40);

        jButton6.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton6.setText("Add Expense");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton6);
        jButton6.setBounds(120, 390, 210, 40);

        jButton10.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton10.setText("Delete");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton10);
        jButton10.setBounds(120, 440, 210, 40);

        jLabel31.setFont(new java.awt.Font("Monotype Corsiva", 0, 36)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("Expenses");
        jPanel4.add(jLabel31);
        jLabel31.setBounds(340, 10, 270, 40);
        jPanel4.add(jDateChooser2);
        jDateChooser2.setBounds(110, 190, 220, 30);

        javax.swing.GroupLayout ExpensesLayout = new javax.swing.GroupLayout(Expenses.getContentPane());
        Expenses.getContentPane().setLayout(ExpensesLayout);
        ExpensesLayout.setHorizontalGroup(
            ExpensesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ExpensesLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 981, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        ExpensesLayout.setVerticalGroup(
            ExpensesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ExpensesLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 621, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1355, 693));
        setPreferredSize(new java.awt.Dimension(1355, 693));
        setSize(new java.awt.Dimension(1366, 768));
        getContentPane().setLayout(null);

        jPanel1.setMinimumSize(new java.awt.Dimension(1360, 768));
        jPanel1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jPanel1FocusGained(evt);
            }
        });
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("jLabel1");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(480, 70, 150, 40);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("jLabel2");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(700, 70, 170, 40);

        jLabel3.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel3.setText("Date:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(420, 80, 60, 23);

        jLabel4.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel4.setText("Time:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(640, 80, 43, 23);

        btnLogout.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });
        jPanel1.add(btnLogout);
        btnLogout.setBounds(20, 400, 160, 40);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Time", "Food", "Drink", "Total Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTable1FocusGained(evt);
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTable1MouseEntered(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(190, 410, 480, 180);

        cbofood.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cbofood.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-select food-" }));
        cbofood.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                cbofoodFocusGained(evt);
            }
        });
        cbofood.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbofoodMouseClicked(evt);
            }
        });
        cbofood.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbofoodActionPerformed(evt);
            }
        });
        jPanel1.add(cbofood);
        cbofood.setBounds(260, 160, 260, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Food:");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(200, 160, 60, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Quantity:");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(540, 260, 80, 30);

        btnPlace.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        btnPlace.setText("Place Order");
        btnPlace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlaceActionPerformed(evt);
            }
        });
        jPanel1.add(btnPlace);
        btnPlace.setBounds(20, 160, 160, 40);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("Total:");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(450, 600, 80, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setText("Drink:");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(200, 260, 60, 30);

        jComboBox2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-select drink-" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });
        jPanel1.add(jComboBox2);
        jComboBox2.setBounds(260, 260, 260, 30);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel11.setText("Price:");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(710, 260, 60, 30);

        txtPrice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPrice.setText("0.0");
        txtPrice.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtPriceCaretUpdate(evt);
            }
        });
        jPanel1.add(txtPrice);
        txtPrice.setBounds(770, 160, 170, 30);

        txtPrice2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPrice2.setText("0.0");
        txtPrice2.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtPrice2CaretUpdate(evt);
            }
        });
        jPanel1.add(txtPrice2);
        txtPrice2.setBounds(770, 260, 170, 30);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setLayout(null);

        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel2.add(jTextField1);
        jTextField1.setBounds(110, 50, 220, 30);

        jTextField2.setEditable(false);
        jTextField2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });
        jPanel2.add(jTextField2);
        jTextField2.setBounds(110, 10, 220, 30);

        jTextField3.setEditable(false);
        jTextField3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel2.add(jTextField3);
        jTextField3.setBounds(110, 90, 220, 30);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel12.setText("Time");
        jPanel2.add(jLabel12);
        jLabel12.setBounds(40, 10, 80, 30);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel14.setText("Food");
        jPanel2.add(jLabel14);
        jLabel14.setBounds(40, 50, 80, 30);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel15.setText("Drink");
        jPanel2.add(jLabel15);
        jLabel15.setBounds(40, 90, 80, 30);

        jButton2.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jButton2.setText("Cancel order");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(100, 180, 160, 30);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel16.setText("Total");
        jPanel2.add(jLabel16);
        jLabel16.setBounds(40, 130, 80, 30);

        jTextField4.setEditable(false);
        jTextField4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel2.add(jTextField4);
        jTextField4.setBounds(110, 130, 220, 30);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(680, 410, 360, 220);

        txtTotal.setEditable(false);
        txtTotal.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        txtTotal.setText("0.00");
        jPanel1.add(txtTotal);
        txtTotal.setBounds(530, 600, 140, 30);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setText("Total:");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(710, 350, 60, 30);

        lblTotal.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        lblTotal.setText("0.00");
        jPanel1.add(lblTotal);
        lblTotal.setBounds(770, 350, 170, 30);

        jButton1.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        jButton1.setText("Process Order");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(20, 280, 160, 40);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel18.setText("Price:");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(710, 160, 60, 30);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel19.setText("Quantity:");
        jPanel1.add(jLabel19);
        jLabel19.setBounds(540, 160, 80, 30);

        Calculator.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        Calculator.setText("Calculator");
        Calculator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalculatorActionPerformed(evt);
            }
        });
        jPanel1.add(Calculator);
        Calculator.setBounds(1110, 100, 160, 40);

        jDesktopPane1.setPreferredSize(new java.awt.Dimension(370, 500));

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 276, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 414, Short.MAX_VALUE)
        );

        jPanel1.add(jDesktopPane1);
        jDesktopPane1.setBounds(1060, 150, 276, 414);

        CustomOrder.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        CustomOrder.setText("Custom Order");
        CustomOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CustomOrderActionPerformed(evt);
            }
        });
        jPanel1.add(CustomOrder);
        CustomOrder.setBounds(20, 220, 160, 40);

        jLabel17.setFont(new java.awt.Font("Castellar", 1, 36)); // NOI18N
        jLabel17.setText("SWEET LULU INVESTMENTS");
        jPanel1.add(jLabel17);
        jLabel17.setBounds(350, 10, 590, 70);

        txtPrice1.setEditable(false);
        txtPrice1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPrice1.setText("0.0");
        txtPrice1.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtPrice1CaretUpdate(evt);
            }
        });
        jPanel1.add(txtPrice1);
        txtPrice1.setBounds(770, 200, 170, 30);

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel20.setText("Total:");
        jPanel1.add(jLabel20);
        jLabel20.setBounds(710, 200, 60, 30);

        txtPrice3.setEditable(false);
        txtPrice3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPrice3.setText("0.0");
        txtPrice3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtPrice3CaretUpdate(evt);
            }
        });
        jPanel1.add(txtPrice3);
        txtPrice3.setBounds(770, 300, 170, 30);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel21.setText("Total:");
        jPanel1.add(jLabel21);
        jLabel21.setBounds(710, 300, 60, 30);

        Quantity2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Quantity2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        Quantity2.setText("1");
        Quantity2.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                Quantity2CaretUpdate(evt);
            }
        });
        jPanel1.add(Quantity2);
        Quantity2.setBounds(620, 260, 60, 30);

        Quantity1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Quantity1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        Quantity1.setText("1");
        Quantity1.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                Quantity1CaretUpdate(evt);
            }
        });
        jPanel1.add(Quantity1);
        Quantity1.setBounds(620, 160, 60, 30);

        btnLogout1.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        btnLogout1.setText("Expenses");
        btnLogout1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnLogout1);
        btnLogout1.setBounds(20, 340, 160, 40);
        jPanel1.add(jDateChooser1);
        jDateChooser1.setBounds(260, 120, 260, 30);

        jLabel33.setText("jLabel33");
        jPanel1.add(jLabel33);
        jLabel33.setBounds(1070, 10, 180, 60);
        jPanel1.add(jDateChooser5);
        jDateChooser5.setBounds(20, 60, 190, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1360, 690);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
    int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to log out?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
if(input == JOptionPane.OK_OPTION){
    
        new Login().setVisible(true);
       this.dispose();
}      
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void cbofoodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbofoodActionPerformed
 Connection con= getConnection();
 if(cbofood.getSelectedItem()=="-select food-"){
    txtPrice.setText("0.00");
    nullItem_price();
}else{  
 try{
        String name=(String) cbofood.getSelectedItem();
        ps = con.prepareStatement("SELECT * FROM `menu` WHERE Name='"+name+"'");
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            txtPrice.setText(rs.getString("Price"));
            nullItem_price();
        } 
    }catch(SQLException e){
        JOptionPane.showMessageDialog(null, e);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
       }
 }
    }//GEN-LAST:event_cbofoodActionPerformed

    private void cbofoodMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbofoodMouseClicked
nullItem_price();
    }//GEN-LAST:event_cbofoodMouseClicked

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
if(jComboBox2.getSelectedItem()=="-select drink-" ){
    txtPrice2.setText("0.00");
    nullItem_price();
}else{   
        Connection con= getConnection();
    try{
        String name=(String) jComboBox2.getSelectedItem();
        ps = con.prepareStatement("SELECT * FROM `drinks` WHERE Name='"+name+"'");
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            txtPrice2.setText(rs.getString("Price"));
            nullItem_price();
        } 
    }catch(SQLException e){
        JOptionPane.showMessageDialog(null, e);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
       }
}
    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void btnPlaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlaceActionPerformed
 Connection con = getConnection();
 // For drinks only
   if(cbofood.getSelectedItem() == "-select food-" && jComboBox2.getSelectedItem() != "-select drink-"){
    String none = "";
    try{
    ps= con.prepareStatement("INSERT INTO `drink_sale`(`Date`, `Time`, `Drink`, `Quantity`, `Total`) VALUES (?,?,?,?,?)");
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser1.getDate());
                ps.setString(1, addDate);
    ps.setString(2, jLabel2.getText());
    ps.setObject(3, jComboBox2.getSelectedItem());
    ps.setInt(4, Integer.parseInt(Quantity2.getText()));
    ps.setDouble(5, Double.parseDouble(txtPrice3.getText()));
    ps.executeUpdate();
    
    ps= con.prepareStatement ("INSERT INTO `meal_sale`(`Time`, `Drink`, `Total`) VALUES (?, ?, ?)");
    ps.setString(1, jLabel2.getText());
    ps.setObject(2, jComboBox2.getSelectedItem());
    ps.setDouble(3, Double.parseDouble(lblTotal.getText()));
    
                     model = (DefaultTableModel) jTable1.getModel();
                    model.addRow(new Object[] {jLabel2.getText(), none, jComboBox2.getSelectedItem(), lblTotal.getText()});
                    txtTotal.setText(Integer.toString(sum.getSum(jTable1, 3)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable1, 3)));
    }catch(SQLException e){
        JOptionPane.showMessageDialog(null, e);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
       }
}
   // For neither food nor drink
   else if(jComboBox2.getSelectedItem()=="-select drink-" && cbofood.getSelectedItem()=="-select food-" ){
     JOptionPane.showMessageDialog(null, "Please select a food or drink");
}
   // For food only
   else if(jComboBox2.getSelectedItem()=="-select drink-" && cbofood.getSelectedItem()!="-select food-" ){
    String nones = "";
    try{
    ps= con.prepareStatement("INSERT INTO `food_sale`(`Date`, `Time`, `Food`, `Quantity`, `Total`) VALUES (?,?,?,?,?)");
     SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser1.getDate());
                ps.setString(1, addDate);
    ps.setString(2, jLabel2.getText());
    ps.setObject(3, cbofood.getSelectedItem());
    ps.setInt(4, Integer.parseInt(Quantity1.getText()));
    ps.setDouble(5, Double.parseDouble(txtPrice1.getText()));
    ps.executeUpdate();
    
    ps= con.prepareStatement ("INSERT INTO `meal_sale`(`Time`, `Food`, `Total`) VALUES (?, ?, ?)");
    ps.setString(1, jLabel2.getText());
    ps.setObject(2, cbofood.getSelectedItem());
    ps.setDouble(3, Double.parseDouble(lblTotal.getText()));
    
                     model = (DefaultTableModel) jTable1.getModel();
                    model.addRow(new Object[] {jLabel2.getText(), cbofood.getSelectedItem(), nones, lblTotal.getText()});
                    txtTotal.setText(Integer.toString(sum.getSum(jTable1, 3)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable1, 3)));
    }catch(SQLException e){
        JOptionPane.showMessageDialog(null, e);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
       }
}
   // For both food and drink
   else if(jComboBox2.getSelectedItem()!="-select drink-" && cbofood.getSelectedItem()!="-select food-" ){
    try{
    ps= con.prepareStatement("INSERT INTO `food_sale`(`Date`, `Time`, `Food`, `Quantity`, `Total`) VALUES (?,?,?,?,?)");
     SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser1.getDate());
                ps.setString(1, addDate);
    ps.setString(2, jLabel2.getText());
    ps.setObject(3, cbofood.getSelectedItem());
    ps.setInt(4, Integer.parseInt(Quantity1.getText()));
    ps.setDouble(5, Double.parseDouble(txtPrice1.getText()));
    ps.executeUpdate();
    
    ps= con.prepareStatement("INSERT INTO `drink_sale`(`Date`, `Time`, `Drink`, `Quantity`, `Total`) VALUES (?,?,?,?,?)");
    ps.setString(1, addDate);
    ps.setString(2, jLabel2.getText());
    ps.setObject(3, jComboBox2.getSelectedItem());
    ps.setInt(4, Integer.parseInt(Quantity1.getText()));
    ps.setDouble(5, Double.parseDouble(txtPrice1.getText()));
    ps.executeUpdate();
    
    ps= con.prepareStatement ("INSERT INTO `meal_sale`(`Time`, `Food`, `Drink`, `Total`) VALUES (?, ?, ?, ?)");
    ps.setString(1, jLabel2.getText());
    ps.setObject(2, cbofood.getSelectedItem());
    ps.setObject(3, jComboBox2.getSelectedItem());
    ps.setDouble(4, Double.parseDouble(lblTotal.getText()));
    
                     model = (DefaultTableModel) jTable1.getModel();
                    model.addRow(new Object[] {jLabel2.getText(), cbofood.getSelectedItem(), jComboBox2.getSelectedItem(), lblTotal.getText()});
                    txtTotal.setText(Integer.toString(sum.getSum(jTable1, 3)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable1, 3)));
    }catch(SQLException e){
        JOptionPane.showMessageDialog(null, e);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
       }
}        
   
    }//GEN-LAST:event_btnPlaceActionPerformed

    private void jPanel1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jPanel1FocusGained
   
    }//GEN-LAST:event_jPanel1FocusGained

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
nullItem_price(); 
jPanel2.setVisible(false);
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        jPanel2.setVisible(true);
        Connection con= getConnection();   
        try{
        model = (DefaultTableModel) jTable1.getModel();
        int selectedRowIndex = jTable1.getSelectedRow();
        jTextField2.setText(model.getValueAt(selectedRowIndex, 0).toString());
        jTextField1.setText(model.getValueAt(selectedRowIndex, 1).toString());
        jTextField3.setText(model.getValueAt(selectedRowIndex, 2).toString());
        jTextField4.setText(model.getValueAt(selectedRowIndex, 3).toString());
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }            
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTable1FocusGained
       jPanel2.setVisible(false);
    }//GEN-LAST:event_jTable1FocusGained

    private void cbofoodFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cbofoodFocusGained

    }//GEN-LAST:event_cbofoodFocusGained

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    Connection con= getConnection();
    try {
                ps = con.prepareStatement("DELETE FROM `food_sale` WHERE `Time` = ?");
                ps.setString(1, jTextField2.getText());
                ps.executeUpdate();
                
                ps = con.prepareStatement("DELETE FROM `drink_sale` WHERE `Time` = ?");
                ps.setString(1, jTextField2.getText());
                ps.executeUpdate();
                
                ps = con.prepareStatement("DELETE FROM `meal_sale` WHERE `Time` = ?");
                ps.setString(1, jTextField2.getText());
                ps.executeUpdate();
                
                 int viewIndex = jTable1.getSelectedRow();
                if (viewIndex != -1) {
                    int modelIndex = jTable1.convertRowIndexToModel(viewIndex);
                    model = (DefaultTableModel)jTable1.getModel();
                    model.removeRow(modelIndex);
                    txtTotal.setText(Integer.toString(sum.getSum(jTable1, 3)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable1, 3)));
                    
                    jTextField2.setText("");
                    jTextField1.setText("");
                    jTextField3.setText("");
                    jTextField4.setText("");
                    jPanel2.setVisible(false);
                }
    } catch (SQLException e) {
    JOptionPane.showMessageDialog(null, e);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
            
        }
    }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTable1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable1MouseEntered

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    SalePayment.setLocationRelativeTo(null);
        SalePayment.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void CalculatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalculatorActionPerformed
      Calculator ca= new Calculator();
       jDesktopPane1.add(ca).setVisible(true);
    }//GEN-LAST:event_CalculatorActionPerformed

    private void Quantity1CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_Quantity1CaretUpdate
//        // TODO add your handling code here:
try {
        double qty = Double.parseDouble(Quantity1.getText());
        double price = Double.parseDouble(txtPrice.getText());
        double total = (qty * price);
        String ttl = String.valueOf(total);
        txtPrice1.setText(ttl);
        } catch (NumberFormatException n) {
//JOptionPane.showMessageDialog(null, n);
}
    }//GEN-LAST:event_Quantity1CaretUpdate

    private void txtPriceCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtPriceCaretUpdate
        // TODO add your handling code here:
try {
        double qty = Double.parseDouble(Quantity1.getText());
        double price = Double.parseDouble(txtPrice.getText());
        double total = (qty * price);
        String ttl = String.valueOf(total);
        txtPrice1.setText(ttl);
} catch (NumberFormatException n) {
//JOptionPane.showMessageDialog(null, n);
}
    }//GEN-LAST:event_txtPriceCaretUpdate

    private void Quantity2CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_Quantity2CaretUpdate
        // TODO add your handling code here:
try {
        double qty = Double.parseDouble(Quantity2.getText());
        double price = Double.parseDouble(txtPrice2.getText());
        double total = (qty * price);
        String ttl = String.valueOf(total);
        txtPrice3.setText(ttl);
        } catch (NumberFormatException n) {
//JOptionPane.showMessageDialog(null, n);
}
    }//GEN-LAST:event_Quantity2CaretUpdate

    private void txtPrice2CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtPrice2CaretUpdate
        // TODO add your handling code here:
try {
        double qty = Double.parseDouble(Quantity2.getText());
        double price = Double.parseDouble(txtPrice2.getText());
        double total = (qty * price);
        String ttl = String.valueOf(total);
        txtPrice3.setText(ttl);
        } catch (NumberFormatException n) {
//JOptionPane.showMessageDialog(null, n);
}
    }//GEN-LAST:event_txtPrice2CaretUpdate

    private void txtPrice1CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtPrice1CaretUpdate
        // TODO add your handling code here:
        try {
        double ttl1 = Double.parseDouble(txtPrice1.getText());
        double ttl2 = Double.parseDouble(txtPrice3.getText());
        double ttl3 = (ttl1 + ttl2);
        String total = String.valueOf(ttl3);
        lblTotal.setText(total);
        } catch (NumberFormatException n) {
//JOptionPane.showMessageDialog(null, n);
}
    }//GEN-LAST:event_txtPrice1CaretUpdate

    private void txtPrice3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtPrice3CaretUpdate
        // TODO add your handling code here:
        try {
        double ttl1 = Double.parseDouble(txtPrice1.getText());
        double ttl2 = Double.parseDouble(txtPrice3.getText());
        double ttl3 = (ttl1 + ttl2);
        String total = String.valueOf(ttl3);
        lblTotal.setText(total);
        } catch (NumberFormatException n) {
//JOptionPane.showMessageDialog(null, n);
}
    }//GEN-LAST:event_txtPrice3CaretUpdate

    private void txtTenderedCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtTenderedCaretUpdate
        // TODO add your handling code here:
        try {
            double a = Double.parseDouble(txtPAmnt.getText());
            double b = Double.parseDouble(txtTendered.getText());
            double c = (a - b);
            double bal = c;
            String balance = String.valueOf(bal);
            txtBal.setText(balance);
        } catch (NumberFormatException n) {
        }
    }//GEN-LAST:event_txtTenderedCaretUpdate

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
       model = (DefaultTableModel) jTable1.getModel();
        try {
            int rowCount = jTable1.getRowCount();
            //Remove rows one by one from the end of the table
            for (int i = rowCount - 1; i >= 0; i --) {
               model.removeRow(i);
            }
            cbofood.setSelectedIndex(0);
            jComboBox2.setSelectedIndex(0);
            txtPAmnt.setText("0.0");
            txtBal.setText("0.0");
            txtTendered.setText("0.0");
            txtTotal.setText("0.0");
            jPanel2.setVisible(false);
        } finally {
            SalePayment.dispose();
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void CustomOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CustomOrderActionPerformed
        Custom_Order.setLocationRelativeTo(null);
        Custom_Order.setVisible(true);
    }//GEN-LAST:event_CustomOrderActionPerformed

    private void jTextField5CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField5CaretUpdate
        // TODO add your handling code here:
        try {
        double qty = Double.parseDouble(jTextField5.getText());
        double price = Double.parseDouble(jTextField8.getText());
        double total = (qty * price);
        String totals = String.valueOf(total);
        jTextField6.setText(totals);
        } catch (NumberFormatException n) {
        }
    }//GEN-LAST:event_jTextField5CaretUpdate

    private void jTextField8CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField8CaretUpdate
        // TODO add your handling code here:
        try {
        double qty = Double.parseDouble(jTextField5.getText());
        double price = Double.parseDouble(jTextField8.getText());
        double total = (qty * price);
        String totals = String.valueOf(total);
        jTextField6.setText(totals);
        } catch (NumberFormatException n) {
        }
    }//GEN-LAST:event_jTextField8CaretUpdate

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        Connection con = getConnection();
        // For drink
         if(jComboBox1.getSelectedItem() == "Drink"){
    String none = "";
    try{
    ps= con.prepareStatement("INSERT INTO `drink_sale`(`Date`, `Time`, `Drink`, `Quantity`, `Total`) VALUES (?,?,?,?,?)");
     SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser1.getDate());
                ps.setString(1, addDate);
    ps.setString(2, jLabel2.getText());
    ps.setString(3, jTextField7.getText());
    ps.setInt(4, Integer.parseInt(jTextField5.getText()));
    ps.setDouble(5, Double.parseDouble(jTextField6.getText()));
    ps.executeUpdate();
    
    ps= con.prepareStatement ("INSERT INTO `meal_sale`(`Time`, `Drink`, `Total`) VALUES (?, ?, ?)");
    ps.setString(1, jLabel2.getText());
    ps.setObject(2, jComboBox1.getSelectedItem());
    ps.setDouble(3, Double.parseDouble(jTextField6.getText()));
    
                     model = (DefaultTableModel) jTable1.getModel();
                    model.addRow(new Object[] {jLabel2.getText(), none, jTextField7.getText(), jTextField6.getText()});
                    txtTotal.setText(Integer.toString(sum.getSum(jTable1, 3)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable1, 3)));
    }catch(SQLException e){
        JOptionPane.showMessageDialog(null, e);
    } finally {
        try {
            con.close();
        } catch (SQLException ex) {
        }
    Custom_Order.dispose();
    }
         } 
         // For food
         if(jComboBox1.getSelectedItem() == "Food"){
         String nones = "";
    try{
    ps= con.prepareStatement("INSERT INTO `food_sale`(`Date`, `Time`, `Food`, `Quantity`, `Total`) VALUES (?,?,?,?,?)");
     SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser1.getDate());
                ps.setString(1, addDate);
    ps.setString(2, jLabel2.getText());
    ps.setString(3, jTextField7.getText());
    ps.setInt(4, Integer.parseInt(jTextField5.getText()));
    ps.setDouble(5, Double.parseDouble(jTextField6.getText()));
    ps.executeUpdate();
    
    ps= con.prepareStatement ("INSERT INTO `meal_sale`(`Time`, `Food`, `Total`) VALUES (?, ?, ?)");
    ps.setString(1, jLabel2.getText());
    ps.setObject(2, jComboBox1.getSelectedItem());
    ps.setDouble(3, Double.parseDouble(jTextField6.getText()));
    
                     model = (DefaultTableModel) jTable1.getModel();
                    model.addRow(new Object[] {jLabel2.getText(), jTextField7.getText(), nones, jTextField6.getText()});
                    txtTotal.setText(Integer.toString(sum.getSum(jTable1, 3)));
                    txtPAmnt.setText(Integer.toString(sum.getSum(jTable1, 3)));
    }catch(SQLException e){
        JOptionPane.showMessageDialog(null, e);
    } finally {
             try {
                 con.close();
             } catch (SQLException ex) {
             }
             jTextField7.setText(null);
             jTextField5.setText(null);
             jTextField6.setText("0.0");
             jTextField8.setText("0.0");
    Custom_Order.dispose();
    }
         }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnLogout1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout1ActionPerformed
        // TODO add your handling code here:
        Expenses.setLocationRelativeTo(null);
        Expenses.setVisible(true);
    }//GEN-LAST:event_btnLogout1ActionPerformed

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        jButton6.setEnabled(false);
        model = (DefaultTableModel) jTable3.getModel();
        int selectedRowIndex = jTable3.getSelectedRow();
        item.setText(model.getValueAt(selectedRowIndex, 0).toString());
        itemprice.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_jTable3MouseClicked

    private void txt_search3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_search3CaretUpdate
        // TODO add your handling code here:
        jTable.FillTable(jTable3, expenses, txt_search3);
        totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
    }//GEN-LAST:event_txt_search3CaretUpdate

    private void txt_search3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_search3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_search3MouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if(From.getDate()==null || To.getDate()==null){
            JOptionPane.showMessageDialog(null, "Set Dates first");
        }else{
            FilterTable(jTable3,te,From.getDate(),To.getDate());
            item.setText("");
            itemprice.setText("");
            totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void itempriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itempriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itempriceActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
        Date();
        item.setText("");
        itemprice.setText("");
        jButton6.setEnabled(true);
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if(item.getText().length()>0 & itemprice.getText().length()>0)
        {
            Connection con = getConnection();
           String namee= item.getText();
           String prc= itemprice.getText();
           SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser2.getDate());
    
        String sq="INSERT INTO `expenses`(`Date` ,`Item`, `Cost`) VALUES (?,?,?)";
       try{
               PreparedStatement st= con.prepareStatement(sq);
               st.setString(1, addDate);
               st.setString(2, namee);
                st.setString(3, prc);
                
                st.executeUpdate();
            JOptionPane.showMessageDialog(null,"Operation Successful");
            FilterTable(jTable3,te,jDateChooser5.getDate(),jDateChooser5.getDate());
            totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
                item.setText("");
                itemprice.setText("");
                Date();
            }catch(SQLException e){
                JOptionPane.showMessageDialog(this, e);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        if(item.getText().length()>0 & itemprice.getText().length()>0)
        {  int row =jTable3.getSelectedRow();
            Connection con= getConnection();
            String dates=(jTable3).getModel().getValueAt(row, 0).toString();
            String items=(jTable3).getModel().getValueAt(row, 1).toString();
            String sqle="DELETE FROM `expenses` WHERE Item= ? AND Date=? ";
            
            int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to DELETE Item?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if(input == JOptionPane.OK_OPTION){
                try{ PreparedStatement str= con.prepareStatement(sqle);
                    str.setString(1, items);
                    str.setString(2, dates);
                    str.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Item deleted");
                    FilterTable(jTable3,te,jDateChooser5.getDate(),jDateChooser5.getDate());
                    totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
                    showDate();
                    item.setText("");
                    itemprice.setText("");
                    jButton6.setEnabled(true);
                }catch(SQLException e){
                    JOptionPane.showMessageDialog(null, e);
                }
            } }
            else{
                JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
            }
    }//GEN-LAST:event_jButton10ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for(javax.swing.UIManager.LookAndFeelInfo info: javax.swing.UIManager.getInstalledLookAndFeels()){
                UIManager.setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
            } 
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmployeeMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmployeeMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmployeeMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmployeeMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EmployeeMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Calculator;
    private javax.swing.JButton CustomOrder;
    private javax.swing.JDialog Custom_Order;
    private javax.swing.JDialog Expenses;
    private com.toedter.calendar.JDateChooser From;
    private javax.swing.JTextField Quantity1;
    private javax.swing.JTextField Quantity2;
    private javax.swing.JDialog SalePayment;
    private com.toedter.calendar.JDateChooser To;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnLogout1;
    private javax.swing.JButton btnPlace;
    private javax.swing.JButton btnPrint;
    private javax.swing.JComboBox<String> cbofood;
    private javax.swing.JTextField item;
    private javax.swing.JTextField itemprice;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser5;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTextField totalExpenses;
    private javax.swing.JLabel txtBal;
    private javax.swing.JLabel txtPAmnt;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtPrice1;
    private javax.swing.JTextField txtPrice2;
    private javax.swing.JTextField txtPrice3;
    private javax.swing.JTextField txtTendered;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txt_search3;
    // End of variables declaration//GEN-END:variables
 
}

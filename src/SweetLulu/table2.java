/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SweetLulu;

import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Charlie
 */
public class table2 {
    
    Database con = new Database();
    public void FillTable(JTable table, String Query)
{
    try
    {
        new Database();
       PreparedStatement pstmt = con.conn.prepareStatement(Query);
 con.rs = pstmt.executeQuery();
        
        //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        int columns = con.rs.getMetaData().getColumnCount();
        while(con.rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = con.rs.getObject(i);
            }
            ((DefaultTableModel) table.getModel()).insertRow(con.rs.getRow()-1,row);
        }

        
      
    }
    catch(Exception e)
    {
        JOptionPane.showMessageDialog(null,e);
    }
}
}

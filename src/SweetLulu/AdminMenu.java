/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SweetLulu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Charlie
 */ 
public class AdminMenu extends javax.swing.JFrame {
Database dbase= new Database();
getSum sum= new getSum();
SearchTable jTable = new SearchTable();
JTable3 jTables = new JTable3();
table2 jTable110 = new table2();
static String AccessType;
    /**
     * Creates new form AdminMenu
     */

String food_sale = "SELECT `Date`, `Food`, `Quantity`, `Total` FROM `food_sale` WHERE `Food` LIKE ?";
String drink_sale = "SELECT `Date`, `Drink`, `Quantity`, `Total` FROM `drink_sale` WHERE `Drink` LIKE ?";
String food_menu = "SELECT `Name`, `Price` FROM `menu` WHERE `Name` LIKE ?";
String drink_menu = "SELECT `Name`, `Price` FROM `drinks` WHERE `Name` LIKE ?";
String expenses = "SELECT `Date`, `Item`, `Cost` FROM `expenses` WHERE `Item` LIKE ?";
String employee = "SELECT * FROM `worker`";
String admin = "SELECT * FROM `admin`";
String te="SELECT `Date` ,`Item`, `Cost` FROM `expenses` WHERE STR_TO_DATE(Date,'%Y-%m-%d') BETWEEN STR_TO_DATE(?,'%Y-%m-%d') AND STR_TO_DATE(?,'%Y-%m-%d')";
String t="SELECT `Date`, `Food`, `Quantity`, `Total` FROM `food_sale` WHERE STR_TO_DATE(Date,'%Y-%m-%d') BETWEEN STR_TO_DATE(?,'%Y-%m-%d') AND STR_TO_DATE(?,'%Y-%m-%d')";
String ts="SELECT `Date`, `Drink`, `Quantity`, `Total` FROM `drink_sale` WHERE STR_TO_DATE(Date,'%Y-%m-%d') BETWEEN STR_TO_DATE(?,'%Y-%m-%d') AND STR_TO_DATE(?,'%Y-%m-%d')";
    Connection con= getConnection();
    
    public AdminMenu() {
        this.setExtendedState(this.getExtendedState() | AdminMenu.MAXIMIZED_BOTH);
    initComponents();
    Date date = new Date();
        jDateChooser5.setDate(date);
        jDateChooser5.setVisible(false);
        FilterTable(tblCustomer,t,jDateChooser5.getDate(),jDateChooser5.getDate());
        FilterTable(tblCustomer1,ts,jDateChooser5.getDate(),jDateChooser5.getDate());
        FilterTable(jTable3,te,jDateChooser5.getDate(),jDateChooser5.getDate());
     lblTotal.setEditable(false);
     jTable.FillTable(jTable2, food_menu, txt_search);
     jTable.FillTable(tblDrinks, drink_menu, search);
     jTable110.FillTable(tblWorker, employee);
     jTable110.FillTable(jTable1, admin);
     lblTotal.setText(Integer.toString(sum.getSum(tblCustomer, 3)));
     lblTotal1.setText(Integer.toString(sum.getSum(tblCustomer1, 3)));
     Date();
        showTime();
    showDate();
    }
    
     Connection getConnection(){
        Connection con=null;
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sweetlulu", "root", "");
            return con;
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(this, ex);
            return null;
        }
    
    }
    public void FilterTable(JTable table, String query, Date date01, Date date02) {
    Connection con = getConnection();
    try {
    PreparedStatement ps = con.prepareStatement(query);
    java.sql.Timestamp date1 = new java.sql.Timestamp(date01.getTime());
    java.sql.Timestamp date2 = new java.sql.Timestamp(date02.getTime());
    ps.setTimestamp(1, date1);
    ps.setTimestamp(2, date2);
   ResultSet rs = ps.executeQuery();
     //To remove previously added rows
        while(table.getRowCount() > 0) 
        {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        }
        int columns = rs.getMetaData().getColumnCount();
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            ((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
        }
    } catch (SQLException ex) {
    JOptionPane.showMessageDialog(null, ex);
    } finally {
      
    }
    }
    
public void showDate(){
    Date d = new Date();
    SimpleDateFormat s= new SimpleDateFormat("dd-MM-yyyy");
   jLabel3.setText(s.format(d));
}

public void Date() {
Date date = new Date();
jDateChooser6.setDate(date);
}

public void showTime(){
    new Timer(0,new ActionListener(){
        @Override
        public void actionPerformed(ActionEvent ae) {
            Date d =new Date();
            SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
          jLabel4.setText(s.format(d));
        }
    }).start();
}

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnCalculator = new javax.swing.JButton();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel9 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        txtName = new javax.swing.JTextField();
        txtprice = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_search = new javax.swing.JTextField();
        btn_update1 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel31 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        name = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        price = new javax.swing.JTextField();
        btnAdd1 = new javax.swing.JButton();
        btn_delete1 = new javax.swing.JButton();
        btn_update2 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblDrinks = new javax.swing.JTable();
        search = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel16 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCustomer = new javax.swing.JTable();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jLabel17 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel18 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        lblTotal = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txt_search1 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jLabel28 = new javax.swing.JLabel();
        jDateChooser4 = new com.toedter.calendar.JDateChooser();
        jButton11 = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblCustomer1 = new javax.swing.JTable();
        jLabel29 = new javax.swing.JLabel();
        lblTotal1 = new javax.swing.JTextField();
        txt_search2 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        From = new com.toedter.calendar.JDateChooser();
        To = new com.toedter.calendar.JDateChooser();
        item = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        itemprice = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        totalExpenses = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        jButton12 = new javax.swing.JButton();
        txt_search3 = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jDateChooser6 = new com.toedter.calendar.JDateChooser();
        jPanel10 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblWorker = new javax.swing.JTable();
        txtUser = new javax.swing.JTextField();
        txtPass = new javax.swing.JTextField();
        btn_add = new javax.swing.JButton();
        btn_Delete = new javax.swing.JButton();
        BTN_update = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        username = new javax.swing.JTextField();
        password = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        cboOption = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jDesktopPane2 = new javax.swing.JDesktopPane();
        btnLogout = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        btnLogout1 = new javax.swing.JButton();
        jDateChooser5 = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1350, 782));
        setPreferredSize(new java.awt.Dimension(1350, 782));
        getContentPane().setLayout(null);

        jPanel1.setAutoscrolls(true);
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel1.setText("Date:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(430, 90, 60, 40);

        jLabel2.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jLabel2.setText("Time:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(630, 90, 50, 40);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("jLabel3");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(490, 90, 130, 40);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("jLabel4");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(690, 90, 140, 40);

        btnCalculator.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        btnCalculator.setText("Calculator");
        btnCalculator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculatorActionPerformed(evt);
            }
        });
        jPanel1.add(btnCalculator);
        btnCalculator.setBounds(1110, 130, 150, 40);

        jDesktopPane1.setPreferredSize(new java.awt.Dimension(370, 500));

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 276, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 414, Short.MAX_VALUE)
        );

        jPanel1.add(jDesktopPane1);
        jDesktopPane1.setBounds(1040, 180, 276, 414);

        jTabbedPane2.setToolTipText("");
        jTabbedPane2.setFont(new java.awt.Font("Monotype Corsiva", 0, 22)); // NOI18N
        jTabbedPane2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane2MouseClicked(evt);
            }
        });

        jPanel2.setLayout(null);

        btnAdd.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        jPanel2.add(btnAdd);
        btnAdd.setBounds(120, 200, 190, 40);

        btn_delete.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });
        jPanel2.add(btn_delete);
        btn_delete.setBounds(120, 270, 190, 40);

        txtName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel2.add(txtName);
        txtName.setBounds(120, 80, 190, 30);

        txtprice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtprice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpriceActionPerformed(evt);
            }
        });
        jPanel2.add(txtprice);
        txtprice.setBounds(120, 140, 190, 30);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setText("Name:");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(40, 80, 80, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setText("Price:");
        jPanel2.add(jLabel10);
        jLabel10.setBounds(40, 140, 80, 30);

        txt_search.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_searchCaretUpdate(evt);
            }
        });
        txt_search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_searchMouseClicked(evt);
            }
        });
        jPanel2.add(txt_search);
        txt_search.setBounds(530, 30, 190, 30);

        btn_update1.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btn_update1.setText("Update");
        btn_update1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_update1ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_update1);
        btn_update1.setBounds(120, 340, 190, 40);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Food", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTable2MouseEntered(evt);
            }
        });
        jScrollPane5.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel2.add(jScrollPane5);
        jScrollPane5.setBounds(370, 80, 530, 420);

        jLabel31.setForeground(java.awt.Color.blue);
        jLabel31.setText("* Search Food");
        jPanel2.add(jLabel31);
        jLabel31.setBounds(730, 30, 140, 20);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Food Menu", jPanel9);

        jPanel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel13MouseClicked(evt);
            }
        });
        jPanel13.setLayout(null);

        name.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel13.add(name);
        name.setBounds(120, 80, 190, 30);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setText("Name:");
        jPanel13.add(jLabel13);
        jLabel13.setBounds(40, 80, 80, 30);

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel22.setText("Price:");
        jPanel13.add(jLabel22);
        jLabel22.setBounds(40, 140, 80, 30);

        price.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        price.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                priceActionPerformed(evt);
            }
        });
        jPanel13.add(price);
        price.setBounds(120, 140, 190, 30);

        btnAdd1.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btnAdd1.setText("Add");
        btnAdd1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdd1ActionPerformed(evt);
            }
        });
        jPanel13.add(btnAdd1);
        btnAdd1.setBounds(120, 200, 190, 40);

        btn_delete1.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btn_delete1.setText("Delete");
        btn_delete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_delete1ActionPerformed(evt);
            }
        });
        jPanel13.add(btn_delete1);
        btn_delete1.setBounds(120, 270, 190, 40);

        btn_update2.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btn_update2.setText("Update");
        btn_update2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_update2ActionPerformed(evt);
            }
        });
        jPanel13.add(btn_update2);
        btn_update2.setBounds(120, 340, 190, 40);

        tblDrinks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drink", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblDrinks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDrinksMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(tblDrinks);
        if (tblDrinks.getColumnModel().getColumnCount() > 0) {
            tblDrinks.getColumnModel().getColumn(0).setResizable(false);
            tblDrinks.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel13.add(jScrollPane6);
        jScrollPane6.setBounds(370, 80, 530, 420);

        search.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                searchCaretUpdate(evt);
            }
        });
        search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchMouseClicked(evt);
            }
        });
        jPanel13.add(search);
        search.setBounds(530, 30, 190, 30);

        jLabel30.setForeground(java.awt.Color.blue);
        jLabel30.setText("* Search Drink");
        jPanel13.add(jLabel30);
        jLabel30.setBounds(730, 30, 140, 20);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Drinks Menu", jPanel11);

        jPanel5.setToolTipText("");
        jPanel5.setFont(new java.awt.Font("Edwardian Script ITC", 0, 36)); // NOI18N

        jPanel12.setLayout(null);

        jTabbedPane1.setFont(new java.awt.Font("Monotype Corsiva", 0, 20)); // NOI18N

        jPanel18.setLayout(null);

        tblCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Food Served", "Quantity", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCustomerMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblCustomer);
        if (tblCustomer.getColumnModel().getColumnCount() > 0) {
            tblCustomer.getColumnModel().getColumn(0).setResizable(false);
            tblCustomer.getColumnModel().getColumn(1).setResizable(false);
            tblCustomer.getColumnModel().getColumn(2).setResizable(false);
            tblCustomer.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel18.add(jScrollPane2);
        jScrollPane2.setBounds(40, 100, 890, 330);
        jPanel18.add(jDateChooser2);
        jDateChooser2.setBounds(370, 10, 170, 30);

        jLabel17.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel17.setText("From");
        jPanel18.add(jLabel17);
        jLabel17.setBounds(323, 10, 40, 30);
        jPanel18.add(jDateChooser1);
        jDateChooser1.setBounds(580, 10, 180, 30);

        jLabel18.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel18.setText("To");
        jPanel18.add(jLabel18);
        jLabel18.setBounds(550, 10, 30, 30);

        jButton3.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jButton3.setText("Search");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel18.add(jButton3);
        jButton3.setBounds(780, 10, 150, 30);

        lblTotal.setEditable(false);
        lblTotal.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        lblTotal.setText("0.00");
        lblTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblTotalActionPerformed(evt);
            }
        });
        jPanel18.add(lblTotal);
        lblTotal.setBounds(740, 430, 190, 40);

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel20.setText("Total:");
        jPanel18.add(jLabel20);
        jLabel20.setBounds(660, 430, 80, 40);

        txt_search1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_search1.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_search1CaretUpdate(evt);
            }
        });
        txt_search1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_search1MouseClicked(evt);
            }
        });
        jPanel18.add(txt_search1);
        txt_search1.setBounds(570, 60, 190, 30);

        jLabel5.setForeground(java.awt.Color.blue);
        jLabel5.setText("* Search Food");
        jPanel18.add(jLabel5);
        jLabel5.setBounds(770, 60, 140, 20);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, 536, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Food", jPanel16);

        jPanel17.setLayout(null);

        jLabel27.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel27.setText("From");
        jPanel17.add(jLabel27);
        jLabel27.setBounds(323, 10, 40, 30);
        jPanel17.add(jDateChooser3);
        jDateChooser3.setBounds(370, 10, 170, 30);

        jLabel28.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel28.setText("To");
        jPanel17.add(jLabel28);
        jLabel28.setBounds(550, 10, 30, 30);
        jPanel17.add(jDateChooser4);
        jDateChooser4.setBounds(580, 10, 180, 30);

        jButton11.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jButton11.setText("Search");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel17.add(jButton11);
        jButton11.setBounds(780, 10, 150, 30);

        tblCustomer1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Drink Served", "Quantity", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCustomer1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCustomer1MouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(tblCustomer1);
        if (tblCustomer1.getColumnModel().getColumnCount() > 0) {
            tblCustomer1.getColumnModel().getColumn(0).setResizable(false);
            tblCustomer1.getColumnModel().getColumn(1).setResizable(false);
            tblCustomer1.getColumnModel().getColumn(2).setResizable(false);
            tblCustomer1.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel17.add(jScrollPane7);
        jScrollPane7.setBounds(40, 100, 890, 330);

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel29.setText("Total:");
        jPanel17.add(jLabel29);
        jLabel29.setBounds(660, 430, 80, 40);

        lblTotal1.setEditable(false);
        lblTotal1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        lblTotal1.setText("0.00");
        lblTotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lblTotal1ActionPerformed(evt);
            }
        });
        jPanel17.add(lblTotal1);
        lblTotal1.setBounds(740, 430, 190, 40);

        txt_search2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_search2.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_search2CaretUpdate(evt);
            }
        });
        txt_search2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_search2MouseClicked(evt);
            }
        });
        jPanel17.add(txt_search2);
        txt_search2.setBounds(570, 60, 190, 30);

        jLabel8.setForeground(java.awt.Color.blue);
        jLabel8.setText("* Search Drink");
        jPanel17.add(jLabel8);
        jLabel8.setBounds(770, 60, 140, 20);

        jTabbedPane1.addTab("Drinks", jPanel17);

        jPanel12.add(jTabbedPane1);
        jTabbedPane1.setBounds(0, 0, 980, 560);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 975, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Progress Report", jPanel5);

        jPanel15.setLayout(null);
        jPanel15.add(From);
        From.setBounds(400, 20, 170, 30);
        jPanel15.add(To);
        To.setBounds(610, 20, 180, 30);

        item.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel15.add(item);
        item.setBounds(110, 170, 220, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Price");
        jPanel15.add(jLabel7);
        jLabel7.setBounds(40, 220, 70, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Item");
        jPanel15.add(jLabel6);
        jLabel6.setBounds(40, 170, 70, 30);

        itemprice.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemprice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itempriceActionPerformed(evt);
            }
        });
        jPanel15.add(itemprice);
        itemprice.setBounds(110, 220, 220, 30);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Expense", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable3);
        if (jTable3.getColumnModel().getColumnCount() > 0) {
            jTable3.getColumnModel().getColumn(0).setResizable(false);
            jTable3.getColumnModel().getColumn(1).setResizable(false);
            jTable3.getColumnModel().getColumn(2).setResizable(false);
        }

        jPanel15.add(jScrollPane1);
        jScrollPane1.setBounds(350, 120, 600, 340);

        jButton6.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton6.setText("Add Expense");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel15.add(jButton6);
        jButton6.setBounds(120, 320, 210, 40);

        jButton10.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton10.setText("Delete");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel15.add(jButton10);
        jButton10.setBounds(120, 370, 210, 40);

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel23.setText("Total:");
        jPanel15.add(jLabel23);
        jLabel23.setBounds(690, 460, 80, 40);

        totalExpenses.setEditable(false);
        totalExpenses.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        totalExpenses.setText("0.00");
        jPanel15.add(totalExpenses);
        totalExpenses.setBounds(770, 460, 180, 40);

        jLabel24.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel24.setText("From");
        jPanel15.add(jLabel24);
        jLabel24.setBounds(360, 20, 40, 30);

        jLabel25.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel25.setText("To");
        jPanel15.add(jLabel25);
        jLabel25.setBounds(580, 20, 30, 30);

        jButton4.setFont(new java.awt.Font("Corbel", 0, 18)); // NOI18N
        jButton4.setText("Search");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel15.add(jButton4);
        jButton4.setBounds(800, 20, 150, 30);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel26.setText("Date");
        jPanel15.add(jLabel26);
        jLabel26.setBounds(40, 120, 70, 30);

        jButton12.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton12.setText("Clear Fields");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel15.add(jButton12);
        jButton12.setBounds(120, 270, 210, 40);

        txt_search3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_search3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_search3CaretUpdate(evt);
            }
        });
        txt_search3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_search3MouseClicked(evt);
            }
        });
        jPanel15.add(txt_search3);
        txt_search3.setBounds(590, 70, 190, 30);

        jLabel32.setForeground(java.awt.Color.blue);
        jLabel32.setText("* Search Item");
        jPanel15.add(jLabel32);
        jLabel32.setBounds(790, 70, 140, 20);
        jPanel15.add(jDateChooser6);
        jDateChooser6.setBounds(110, 120, 220, 30);

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Expenses", jPanel14);

        jPanel3.setLayout(null);

        tblWorker.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Username", "Password"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblWorker.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblWorkerMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblWorker);
        if (tblWorker.getColumnModel().getColumnCount() > 0) {
            tblWorker.getColumnModel().getColumn(0).setResizable(false);
            tblWorker.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel3.add(jScrollPane3);
        jScrollPane3.setBounds(430, 60, 510, 420);

        txtUser.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel3.add(txtUser);
        txtUser.setBounds(160, 60, 220, 30);

        txtPass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel3.add(txtPass);
        txtPass.setBounds(160, 110, 220, 30);

        btn_add.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });
        jPanel3.add(btn_add);
        btn_add.setBounds(160, 170, 220, 40);

        btn_Delete.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        btn_Delete.setText("Delete");
        btn_Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DeleteActionPerformed(evt);
            }
        });
        jPanel3.add(btn_Delete);
        btn_Delete.setBounds(160, 310, 220, 40);

        BTN_update.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        BTN_update.setText("Update");
        BTN_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTN_updateActionPerformed(evt);
            }
        });
        jPanel3.add(BTN_update);
        BTN_update.setBounds(160, 240, 220, 40);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel11.setText("Username");
        jPanel3.add(jLabel11);
        jLabel11.setBounds(30, 60, 130, 30);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel12.setText("Password");
        jPanel3.add(jLabel12);
        jLabel12.setBounds(30, 110, 130, 30);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Employee Accounts", jPanel10);

        jPanel7.setLayout(null);

        username.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        username.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usernameActionPerformed(evt);
            }
        });
        jPanel7.add(username);
        username.setBounds(160, 60, 220, 30);

        password.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel7.add(password);
        password.setBounds(160, 110, 220, 30);

        jButton7.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton7.setText("Delete");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton7);
        jButton7.setBounds(160, 310, 220, 40);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel19.setText("Username");
        jPanel7.add(jLabel19);
        jLabel19.setBounds(30, 60, 110, 30);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel21.setText("Password");
        jPanel7.add(jLabel21);
        jLabel21.setBounds(30, 110, 110, 30);

        jButton8.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton8.setText("Update");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton8);
        jButton8.setBounds(160, 240, 220, 40);

        jButton9.setFont(new java.awt.Font("Corbel", 0, 22)); // NOI18N
        jButton9.setText("Add");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel7.add(jButton9);
        jButton9.setBounds(160, 170, 220, 40);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Username", "Password"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
        }

        jPanel7.add(jScrollPane4);
        jScrollPane4.setBounds(430, 60, 510, 420);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Admin Accounts", jPanel8);

        jPanel6.setLayout(null);

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SweetLulu/happy.jpg"))); // NOI18N
        jPanel6.add(jLabel14);
        jLabel14.setBounds(10, 0, 70, 60);

        cboOption.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cboOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Do you want to view the report progress of all transactions?", "Do you want to access the Menu?", "Do you want to create an account for your employee or Admin?" }));
        jPanel6.add(cboOption);
        cboOption.setBounds(80, 30, 450, 30);

        jLabel15.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel15.setText("Need Help?  Don't worry how may i assist you?");
        jPanel6.add(jLabel15);
        jLabel15.setBounds(90, 0, 440, 30);

        jButton2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jButton2.setText("Help");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel6.add(jButton2);
        jButton2.setBounds(550, 30, 180, 30);

        javax.swing.GroupLayout jDesktopPane2Layout = new javax.swing.GroupLayout(jDesktopPane2);
        jDesktopPane2.setLayout(jDesktopPane2Layout);
        jDesktopPane2Layout.setHorizontalGroup(
            jDesktopPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 940, Short.MAX_VALUE)
        );
        jDesktopPane2Layout.setVerticalGroup(
            jDesktopPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 450, Short.MAX_VALUE)
        );

        jPanel6.add(jDesktopPane2);
        jDesktopPane2.setBounds(10, 70, 940, 450);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Help", jPanel4);

        jPanel1.add(jTabbedPane2);
        jTabbedPane2.setBounds(40, 150, 980, 600);

        btnLogout.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });
        jPanel1.add(btnLogout);
        btnLogout.setBounds(1190, 620, 140, 40);

        jLabel16.setFont(new java.awt.Font("Castellar", 1, 36)); // NOI18N
        jLabel16.setText("SWEET LULU INVESTMENTS");
        jPanel1.add(jLabel16);
        jLabel16.setBounds(350, 10, 590, 70);

        btnLogout1.setFont(new java.awt.Font("Monotype Corsiva", 0, 24)); // NOI18N
        btnLogout1.setText("Make Order");
        btnLogout1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogout1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnLogout1);
        btnLogout1.setBounds(1030, 620, 150, 40);
        jPanel1.add(jDateChooser5);
        jDateChooser5.setBounds(20, 60, 190, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, -10, 1360, 780);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to log out?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
if(input == JOptionPane.OK_OPTION){
    
        new Login().setVisible(true);
       this.dispose();      
}      
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnCalculatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculatorActionPerformed
       Calculator ca= new Calculator();
       jDesktopPane1.add(ca).setVisible(true);
    }//GEN-LAST:event_btnCalculatorActionPerformed

    private void tblCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCustomerMouseClicked
 
    }//GEN-LAST:event_tblCustomerMouseClicked

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
 if(txtName.getText().length()>0 & txtprice.getText().length()>0) 
     {
          
       int row =jTable2.getSelectedRow();
        String chaly=(jTable2).getModel().getValueAt(row, 0).toString();
        String sqle="DELETE FROM `menu` WHERE name='"+chaly+"'";
       try{
            int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to DELETE row?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if(input == JOptionPane.OK_OPTION){
           dbase.st.execute(sqle);
           JOptionPane.showMessageDialog(null, "Item deleted");
               jTable.FillTable(jTable2, food_menu, txt_search);
               txtName.setText("");
               txtprice.setText("");
}
           }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
       }
     } 
   else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }    
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        if(txtName.getText().length()>0 & txtprice.getText().length()>0) 
        {  
            String name=txtName.getText();
           String prc= txtprice.getText();
    
        String sq="INSERT INTO `menu`(`Name`, `Price`) VALUES ('"+name+"','"+prc+"')";
       try{
               dbase.st.execute(sq);
          
              JOptionPane.showMessageDialog(null,"Operation Successful");
            
       }catch(SQLException e){
          JOptionPane.showMessageDialog(this, e);
       }
       jTable.FillTable(jTable2, food_menu, txt_search);
               txtName.setText("");
               txtprice.setText("");
        }else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }
    }//GEN-LAST:event_btnAddActionPerformed

    private void txtpriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpriceActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
      if(txtUser.getText().length()>0 & txtPass.getText().length()>0) 
        {  
            String User=txtUser.getText();
           String Pass= txtPass.getText();
    
        String w="INSERT INTO `worker`(`Ussername`, `Password`) VALUES ('"+User+"','"+Pass+"')";
       try{
               dbase.st.execute(w);
          
              JOptionPane.showMessageDialog(null,"Operation Successful");
               jTable110.FillTable(tblWorker, employee);
               txtUser.setText("");
               txtPass.setText("");
            
       }catch(SQLException e){
          JOptionPane.showMessageDialog(this, e);
       }
        }else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }
    }//GEN-LAST:event_btn_addActionPerformed

    private void tblWorkerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblWorkerMouseClicked
        DefaultTableModel model = (DefaultTableModel) tblWorker.getModel();
        int selectedRowIndex = tblWorker.getSelectedRow();
        txtUser.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtPass.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_tblWorkerMouseClicked

    private void txt_searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_searchMouseClicked

       
    }//GEN-LAST:event_txt_searchMouseClicked

    private void btn_DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DeleteActionPerformed
      if(txtUser.getText().length()>0 & txtPass.getText().length()>0) 
     {
          
       int row =tblWorker.getSelectedRow();
        String chaly=(tblWorker).getModel().getValueAt(row, 0).toString();
        String sqle="DELETE FROM `worker` WHERE Ussername='"+chaly+"'";
       try{
            int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to DELETE row?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if(input == JOptionPane.OK_OPTION){
                dbase.st.execute(sqle);
                JOptionPane.showMessageDialog(null, "Item deleted");
               jTable110.FillTable(tblWorker, employee);
               txtUser.setText("");
               txtPass.setText("");
}
           }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
       }
     } 
   else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }    
    }//GEN-LAST:event_btn_DeleteActionPerformed

    private void jTabbedPane2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane2MouseClicked

        txtName.setText("");
        txtprice.setText("");
         txtUser.setText("");
         txtPass.setText("");
         username.setText("");
         password.setText("");
         item.setText("");
         itemprice.setText("");
       name.setText("");
       price.setText("");
       showDate();
    }//GEN-LAST:event_jTabbedPane2MouseClicked

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
if(jDateChooser2.getDate()==null || jDateChooser1.getDate()==null){
    JOptionPane.showMessageDialog(null, "Set Dates first");
   }else{
     FilterTable(tblCustomer,t,jDateChooser2.getDate(),jDateChooser1.getDate());
     lblTotal.setText(Integer.toString(sum.getSum(tblCustomer, 3)));
}
    }//GEN-LAST:event_jButton3ActionPerformed

    private void usernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_usernameActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        String user= username.getText();
        String pass= password.getText();
        if(user.length()>0 & pass.length()>0){
            Connection conn= getConnection();
            try{
                PreparedStatement ts= conn.prepareStatement("INSERT INTO `admin`(`Username`, `Password`) VALUES (?,?)");
                ts.setString(1, user);
                ts.setString(2, pass);
                ts.executeUpdate();
                 JOptionPane.showMessageDialog(null, "Operation successful");
                 jTable110.FillTable(jTable1, admin);
                 username.setText("");
                 password.setText("");
            }catch(SQLException e){
                JOptionPane.showMessageDialog(null, e);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Empty fields sumected");
        }
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
         
        if(username.getText().length()>0 & password.getText().length()>0)
  {    int row =jTable1.getSelectedRow();
      String ch=(jTable1).getModel().getValueAt(row, 0).toString();
    String User=username.getText();
     String Pass=password.getText();
        String sqe="UPDATE `admin` SET`Username`='"+User+"',`Password`='"+Pass+"'WHERE Username='"+ch+"'";
        Database ss= new Database();
        try{
            ss.st.execute(sqe);
            JOptionPane.showMessageDialog(null, "Successfully updated");
               jTable110.FillTable(jTable1, admin);
               username.setText("");
               password.setText("");
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
            
        }
        }else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }              
        
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
         if(username.getText().length()>0 & password.getText().length()>0)
     {
          
       int row =jTable1.getSelectedRow();
        String chaly=(jTable1).getModel().getValueAt(row, 0).toString();
        String sqle="DELETE FROM `admin` WHERE Username='"+chaly+"'";
       try{
            int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to DELETE row?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if(input == JOptionPane.OK_OPTION){
                dbase.st.execute(sqle);
                JOptionPane.showMessageDialog(null, "Item deleted");
              jTable110.FillTable(jTable1, admin);
               username.setText("");
               password.setText("");
}
           }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
       }
     } 
   else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }     
      
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int selectedRowIndex = jTable1.getSelectedRow();
        username.setText(model.getValueAt(selectedRowIndex, 0).toString());
        password.setText(model.getValueAt(selectedRowIndex, 1).toString());           
    }//GEN-LAST:event_jTable1MouseClicked

    private void BTN_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTN_updateActionPerformed
if(txtUser.getText().length()>0 & txtPass.getText().length()>0)
  {    int row =tblWorker.getSelectedRow();
      String ch=(tblWorker).getModel().getValueAt(row, 0).toString();
    String User=txtUser.getText();
     String Pass=txtPass.getText();
        String sqe="UPDATE `worker` SET`Ussername`='"+User+"',`Password`='"+Pass+"'WHERE Ussername='"+ch+"'";
        Database ss= new Database();
        try{
            ss.st.execute(sqe);
            JOptionPane.showMessageDialog(null, "Successfully updated");
               jTable110.FillTable(tblWorker, employee);
               txtUser.setText("");
               txtPass.setText("");
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }}else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }              
    }//GEN-LAST:event_BTN_updateActionPerformed

    private void btn_update1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_update1ActionPerformed
   if(txtName.getText().length()>0 & txtprice.getText().length()>0)
  {    int row =jTable2.getSelectedRow();
      String ch=(jTable2).getModel().getValueAt(row, 0).toString();
    String User=txtName.getText();
     String Pass=txtprice.getText();
        String sq="UPDATE `menu` SET`Name`='"+User+"',`Price`='"+Pass+"'WHERE Name='"+ch+"'";
        Database s= new Database();
        try{
            s.st.execute(sq);
            JOptionPane.showMessageDialog(null, "Successfully updated");
               jTable.FillTable(jTable2, food_menu, txt_search);
               txtName.setText("");
               txtprice.setText("");
        }catch(SQLException e){
            e.getMessage();
        }}else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }        
    }//GEN-LAST:event_btn_update1ActionPerformed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
        int selectedRowIndex = jTable2.getSelectedRow();
        txtName.setText(model.getValueAt(selectedRowIndex, 0).toString());
        txtprice.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_jTable2MouseClicked

    private void lblTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblTotalActionPerformed

    private void priceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_priceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_priceActionPerformed

    private void btnAdd1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdd1ActionPerformed
        if(name.getText().length()>0 & price.getText().length()>0) 
        {  
            String namee= name.getText();
           String prc= price.getText();
    
        String sq="INSERT INTO `drinks`(`Name`, `Price`) VALUES (?,?)";
       try{
               PreparedStatement st= con.prepareStatement(sq);
               st.setString(1, namee);
                st.setString(2, prc);
                st.executeUpdate();
            JOptionPane.showMessageDialog(null,"Operation Successful");
            
       }catch(SQLException e){
          JOptionPane.showMessageDialog(this, e);
       }
       jTable.FillTable(tblDrinks, drink_menu, search);
       name.setText("");
       price.setText("");
        }else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }
    }//GEN-LAST:event_btnAdd1ActionPerformed

    private void btn_delete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_delete1ActionPerformed
    if(name.getText().length()>0 & price.getText().length()>0) 
     {  int row =tblDrinks.getSelectedRow();
        String chaly=(tblDrinks).getModel().getValueAt(row, 0).toString();
        String sqle="DELETE FROM `drinks` WHERE Nane= ? ";
      
            int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to DELETE row?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if(input == JOptionPane.OK_OPTION){
             try{ PreparedStatement str= con.prepareStatement(sqle);
            str.setString(1, chaly);
            str.executeUpdate();
                JOptionPane.showMessageDialog(null, "Item deleted");
                jTable.FillTable(tblDrinks, drink_menu, search);
                name.setText("");
                price.setText("");
            }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
       }
     } }
   else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }    
    }//GEN-LAST:event_btn_delete1ActionPerformed

    private void btn_update2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_update2ActionPerformed
         if(name.getText().length()>0 & price.getText().length()>0)
  {    int row =tblDrinks.getSelectedRow();
      String ch=(tblDrinks).getModel().getValueAt(row, 0).toString();
    String User=name.getText();
     String Pass=price.getText();
        String sq="UPDATE `drinks` SET `Name`= ? ,`Price`= ? WHERE Nane='"+ch+"'";
        try{
            PreparedStatement ps= con.prepareStatement(sq);
            ps.setString(1, User);
            ps.setString(2, Pass);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Successfully updated");
            jTable.FillTable(tblDrinks, drink_menu, search);
            name.setText("");
             price.setText("");
        }catch(SQLException e){
            e.getMessage();
        }}else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }        
    }//GEN-LAST:event_btn_update2ActionPerformed

    private void tblDrinksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDrinksMouseClicked
         DefaultTableModel model = (DefaultTableModel) tblDrinks.getModel();
        int selectedRowIndex = tblDrinks.getSelectedRow();
        name.setText(model.getValueAt(selectedRowIndex, 0).toString());
        price.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_tblDrinksMouseClicked

    private void searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_searchMouseClicked

    private void jTable2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable2MouseEntered

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
       if(item.getText().length()>0 & itemprice.getText().length()>0) 
        {  
            String namee= item.getText();
           String prc= itemprice.getText();
           SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = date.format(jDateChooser6.getDate());
    
        String sq="INSERT INTO `expenses`(`Date` ,`Item`, `Cost`) VALUES (?,?,?)";
       try{
               PreparedStatement st= con.prepareStatement(sq);
               st.setString(1, addDate);
               st.setString(2, namee);
                st.setString(3, prc);
                
                st.executeUpdate();
            JOptionPane.showMessageDialog(null,"Operation Successful");
            FilterTable(jTable3,te,jDateChooser5.getDate(),jDateChooser5.getDate());
            totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
       item.setText("");
       itemprice.setText("");
       Date();
       }catch(SQLException e){
          JOptionPane.showMessageDialog(this, e);
        }
  }else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
       }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        jButton6.setEnabled(false);
        DefaultTableModel model = (DefaultTableModel) jTable3.getModel();
        int selectedRowIndex = jTable3.getSelectedRow();
        item.setText(model.getValueAt(selectedRowIndex, 0).toString());
        itemprice.setText(model.getValueAt(selectedRowIndex, 1).toString());
    }//GEN-LAST:event_jTable3MouseClicked

    private void jPanel13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel13MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel13MouseClicked

    private void itempriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itempriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itempriceActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
       if(item.getText().length()>0 & itemprice.getText().length()>0) 
     {  int row =jTable3.getSelectedRow();
        Connection con= getConnection();
            String dates=(jTable3).getModel().getValueAt(row, 0).toString();
            String items=(jTable3).getModel().getValueAt(row, 1).toString();
            String sqle="DELETE FROM `expenses` WHERE Item= ? AND Date=? ";
            
            int input = JOptionPane.showConfirmDialog(null,"Are you sure you want to DELETE Item?  ","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if(input == JOptionPane.OK_OPTION){
                try{ PreparedStatement str= con.prepareStatement(sqle);
                    str.setString(1, items);
                    str.setString(2, dates);
                    str.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Item deleted");
               FilterTable(jTable3,te,jDateChooser5.getDate(),jDateChooser5.getDate());
                totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
    showDate();
                item.setText("");
                itemprice.setText("");
                jButton6.setEnabled(true);
                Date();
            }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
       }
     } }
   else{
      JOptionPane.showMessageDialog(null, "Empty Fields Detected!!");
  }    
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(cboOption.getSelectedItem()=="Do you want to view the report progress of all transactions?"){
            ViewCustomers view = new ViewCustomers();
            jDesktopPane2.add(view).setVisible(true);
        }else  if(cboOption.getSelectedItem()=="Do you want to access the Menu?"){
            FoodMenu food = new FoodMenu();
            jDesktopPane2.add(food).setVisible(true);
        }else  if(cboOption.getSelectedItem()=="Do you want to create an account for your employee or Admin?"){
            AddEmployee foo = new AddEmployee();
            jDesktopPane2.add(foo).setVisible(true);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if(From.getDate()==null || To.getDate()==null){
    JOptionPane.showMessageDialog(null, "Set Dates first");
   }else{
     FilterTable(jTable3,te,From.getDate(),To.getDate());
       item.setText("");
       itemprice.setText("");
totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
}
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
        Date();
                item.setText("");
                itemprice.setText("");
                jButton6.setEnabled(true);
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        if(jDateChooser2.getDate()==null || jDateChooser1.getDate()==null){
    JOptionPane.showMessageDialog(null, "Set Dates first");
   }else{
     FilterTable(tblCustomer1,ts,jDateChooser3.getDate(),jDateChooser4.getDate());
         lblTotal1.setText(Integer.toString(sum.getSum(tblCustomer1, 3)));
        }
    }//GEN-LAST:event_jButton11ActionPerformed

    private void tblCustomer1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCustomer1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblCustomer1MouseClicked

    private void lblTotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lblTotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lblTotal1ActionPerformed

    private void txt_search1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_search1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_search1MouseClicked

    private void txt_search2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_search2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_search2MouseClicked

    private void txt_search3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_search3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_search3MouseClicked

    private void btnLogout1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogout1ActionPerformed
        // TODO add your handling code here:
        AccessType = "Administrator";
        EmployeeMenu employee_menu = new EmployeeMenu(AccessType);
        employee_menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnLogout1ActionPerformed

    private void txt_search1CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_search1CaretUpdate
        // TODO add your handling code here:
        jTable.FillTable(tblCustomer, food_sale, txt_search1);
        lblTotal.setText(Integer.toString(sum.getSum(tblCustomer, 3)));
    }//GEN-LAST:event_txt_search1CaretUpdate

    private void txt_search2CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_search2CaretUpdate
        // TODO add your handling code here:
        jTable.FillTable(tblCustomer1, drink_sale, txt_search2);
         lblTotal1.setText(Integer.toString(sum.getSum(tblCustomer1, 3)));
    }//GEN-LAST:event_txt_search2CaretUpdate

    private void txt_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_searchCaretUpdate
        // TODO add your handling code here:
        jTable.FillTable(jTable2, food_menu, txt_search);
    }//GEN-LAST:event_txt_searchCaretUpdate

    private void searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_searchCaretUpdate
        // TODO add your handling code here:
        jTable.FillTable(tblDrinks, drink_menu, search);
    }//GEN-LAST:event_searchCaretUpdate

    private void txt_search3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_search3CaretUpdate
        // TODO add your handling code here:
        jTable.FillTable(jTable3, expenses, txt_search3);
        totalExpenses.setText(Integer.toString(sum.getSum(jTable3, 2)));
    }//GEN-LAST:event_txt_search3CaretUpdate

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For sumails see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for(javax.swing.UIManager.LookAndFeelInfo info: javax.swing.UIManager.getInstalledLookAndFeels()){
                UIManager.setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AdminMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AdminMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AdminMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AdminMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AdminMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTN_update;
    private com.toedter.calendar.JDateChooser From;
    private com.toedter.calendar.JDateChooser To;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAdd1;
    private javax.swing.JButton btnCalculator;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnLogout1;
    private javax.swing.JButton btn_Delete;
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_delete1;
    private javax.swing.JButton btn_update1;
    private javax.swing.JButton btn_update2;
    private javax.swing.JComboBox<String> cboOption;
    private javax.swing.JTextField item;
    private javax.swing.JTextField itemprice;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private com.toedter.calendar.JDateChooser jDateChooser4;
    private com.toedter.calendar.JDateChooser jDateChooser5;
    private com.toedter.calendar.JDateChooser jDateChooser6;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JDesktopPane jDesktopPane2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextField lblTotal;
    private javax.swing.JTextField lblTotal1;
    private javax.swing.JTextField name;
    private javax.swing.JTextField password;
    private javax.swing.JTextField price;
    private javax.swing.JTextField search;
    private javax.swing.JTable tblCustomer;
    private javax.swing.JTable tblCustomer1;
    private javax.swing.JTable tblDrinks;
    private javax.swing.JTable tblWorker;
    private javax.swing.JTextField totalExpenses;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPass;
    private javax.swing.JTextField txtUser;
    private javax.swing.JTextField txt_search;
    private javax.swing.JTextField txt_search1;
    private javax.swing.JTextField txt_search2;
    private javax.swing.JTextField txt_search3;
    private javax.swing.JTextField txtprice;
    private javax.swing.JTextField username;
    // End of variables declaration//GEN-END:variables
}

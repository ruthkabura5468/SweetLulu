/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SweetLulu;

import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author Charlie
 */
public class FoodMenu extends javax.swing.JInternalFrame {

    /**
     * Creates new form FoodMenu
     */
    public FoodMenu() {
       this.setResizable(false);
        initComponents();
    Toolkit tk = Toolkit.getDefaultToolkit();
        int width=(int) tk.getScreenSize().getWidth();
        int height =(int) tk.getScreenSize().getHeight();
        this.setSize(940,440);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setAutoscrolls(true);
        setMaximumSize(new java.awt.Dimension(940, 440));
        setMinimumSize(new java.awt.Dimension(940, 440));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(940, 440));
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);
        getContentPane().add(jPanel1);
        jPanel1.setBounds(46, 0, 0, 408);

        jPanel2.setMaximumSize(new java.awt.Dimension(940, 440));
        jPanel2.setMinimumSize(new java.awt.Dimension(940, 440));
        jPanel2.setName(""); // NOI18N
        jPanel2.setPreferredSize(new java.awt.Dimension(940, 440));
        jPanel2.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("  Hi, follow me;");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(90, 60, 190, 20);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SweetLulu/happy.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(10, 20, 70, 60);

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("To view your menu, click on the tab called Food Menu:");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(20, 100, 730, 20);

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel7.setText("There, you will be able to view all the items on the menu. You can use the seach button to get the name of food you want");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(20, 120, 860, 20);

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel4.setText("You also have the option to delete, update or add an item");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(20, 140, 730, 20);

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 255, 51));
        jLabel5.setText("Get Started");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel5);
        jLabel5.setBounds(20, 160, 730, 20);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 940, 440);
        jPanel2.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        this.setVisible(false);
        new AdminMenu().setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jLabel5MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
